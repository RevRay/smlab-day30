package ru.smlab.day30;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Chef {

    Logger log = LogManager.getRootLogger();

    /**
     * Если в наборе нету хотябы двух булочек и котлеты, либо булочек больше 3 - набор должен быть отбракован
     * @param pack
     * @return
     */
    public boolean validatePackage(Package pack) {
        List<Ingredients> ingredients = pack.getUnsortedBurgerIngredients();

        long bunCount = ingredients.stream()
                .filter(i -> i.equals(Ingredients.BUN)).count();

        //1. проверяем что в упаковке две или три булочки.
        boolean isValid = bunCount == 2 || bunCount == 3;
        log.debug("Упаковка провалидирована, результат: " + isValid);
        return isValid;
        //2. TODO проверяем что в упаковке есть котлета
    }


    //TODO public Burger cookBurger(Package p) - метод приготовления бургера


    public List<Package> getInvalidPackages(List<Package> packages) {
        return packages.stream().filter(p -> !validatePackage(p)).collect(Collectors.toList());
    }

    public List<Package> getValidPackages(List<Package> packages) {
        return packages.stream().filter(p -> validatePackage(p)).collect(Collectors.toList());
    }

}
