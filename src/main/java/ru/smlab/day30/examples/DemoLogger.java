package ru.smlab.day30.examples;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DemoLogger {

    static Logger log = LogManager.getRootLogger();
    static Logger log2 = LogManager.getLogger(DemoLogger.class);

    public static void main(String[] args) {
        System.out.println("hi");

        log2.info("hi");

        try {
            int i = 2 / 0;
        } catch (ArithmeticException e) {
            log2.error("division by zero");
            log2.fatal("division by zero");
        }

        log2.debug("finishing program execution");
    }
}
