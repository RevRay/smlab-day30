package ru.smlab.day30;

public enum Ingredients {
    //огурцы, помидоры, котлета, сыр, кетчуп, майонез, булочка, салат
    CUCUMBER,
    TOMATO,
    BEEF,
    CHEESE,
    KETCHUP,
    MAYONAISE,
    BUN,
    SALAD
}
