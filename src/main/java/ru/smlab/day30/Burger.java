package ru.smlab.day30;

import java.util.List;
import java.util.Set;

public class Burger {
    List<Ingredients> burgerIngredients;

    public Burger(List<Ingredients> burgerIngredients) {
        this.burgerIngredients = burgerIngredients;
    }

    @Override
    public String toString() {
        return "Burger{" +
                "burgerIngredients=" + burgerIngredients +
                '}';
    }
}
