package ru.smlab.day30;

import java.util.List;

public class Package {
    List<Ingredients> unsortedBurgerIngredients;

    public List<Ingredients> getUnsortedBurgerIngredients() {
        return unsortedBurgerIngredients;
    }

    public Package(List<Ingredients> burgerIngredients) {
        this.unsortedBurgerIngredients = burgerIngredients;
    }

    @Override
    public String toString() {
        return "Package{" +
                "burgerIngredients=" + unsortedBurgerIngredients +
                '}';
    }
}
