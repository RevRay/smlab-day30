package ru.smlab.day30;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PackageFactory {
    static Random rnd = new Random();
    static Ingredients[] availableIngredients = Ingredients.values();
    Logger log = LogManager.getRootLogger();

    public Package generatePackage() {
        int ingredientsAmount = rnd.nextInt(10) + 1;
        List<Ingredients> generatedIngredients = new ArrayList<>();

        for (int i = 0; i < ingredientsAmount; i++) {
            int randomIngredientIndex = rnd.nextInt(availableIngredients.length);
            generatedIngredients.add(availableIngredients[randomIngredientIndex]);
        }

        Package p = new Package(generatedIngredients);
        log.info("сгенерировал упаковку " +p);
        return p;
    }

    public List<Package> generatePackages(int packageCount) {
        List<Package> generatedPackages = new ArrayList<>();
        for (int i = 0; i < packageCount; i++) {
            generatedPackages.add(generatePackage());
        }
        return generatedPackages;
    }
}
